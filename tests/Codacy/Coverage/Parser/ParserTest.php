<?php

namespace Codacy\Coverage\Parser;


use Codacy\Coverage\Util\JsonProducer;

class ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Running against tests/res/phpunit-clover
     *
     * THIS TEST DOES NOT WORK CORRECTLY YET
     */
    public function testParsersProduceSameResult()
    {
        $codacyXmlSource = '/Users/antoinettesmith/projects/php-codacy-coverage-mod/tests/res/phpunitxml/index.xml';
        $dtXml = '/Users/antoinettesmith/projects/php-codacy-coverage-mod/tests/res/build/index.xml';

        $xunitParser = new PhpUnitXmlParser($dtXml, '/home/jacke/Desktop/codacy-php');
        $xunitParser->setDirOfFileXmls('/Users/antoinettesmith/projects/php-codacy-coverage-mod/tests/res/build');
        $expectedJson = file_get_contents('/Users/antoinettesmith/projects/php-codacy-coverage-mod/tests/res/expected.json', true);

        $jsonProducer = new JsonProducer();

        $jsonProducer->setParser($xunitParser);

        $xunitJson = $jsonProducer->makeJson();

        $this->assertJsonStringEqualsJsonString($expectedJson, $xunitJson);
    }
}

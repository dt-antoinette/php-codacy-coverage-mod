<?php

use Codacy\Coverage\Parser\PhpUnitXmlParser;

class PhpUnitXmlParserTest extends PHPUnit_Framework_TestCase
{


    public function testThrowsExceptionOnWrongPath()
    {
        $this->setExpectedException('InvalidArgumentException');
        new PhpUnitXmlParser("/home/foo/bar/baz/fake.xml");
    }

    /**
     *
     * The test had been made in /home/jacke/Desktop/codacy-php so we need to pass this
     * as 2nd (optional) parameter. Otherwise the filename will not be correct and test
     * would fail on other machines or in other directories.
     */
    public function testCanParsePhpUnitXmlReport()
    {
        $codacyXmlSource = '/Users/antoinettesmith/projects/php-codacy-coverage-mod/tests/res/phpunitxml/index.xml';
        $dtXml = '/Users/antoinettesmith/projects/php-codacy-coverage-mod/tests/res/build/index.xml';

        $parser = new PhpUnitXmlParser($dtXml, '/home/jacke/Desktop/codacy-php');
        $parser->setDirOfFileXmls('/Users/antoinettesmith/projects/php-codacy-coverage-mod/tests/res/build');
        $report = $parser->makeReport();

        //$this->assertEquals(69, $report->getTotal());
        //$this->assertEquals(10, sizeof($report->getFileReports()));

        $fileReports = $report->getFileReports();

        // @TODO: Test this line coverage

        //$configFileName = $configFileReport->getFileName();

    }

}